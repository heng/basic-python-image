FROM ubuntu:bionic

# Existing lsb_release causes issues with modern installations of Python3
# https://github.com/pypa/pip/issues/4924#issuecomment-435825490
# Set (temporarily) DEBIAN_FRONTEND to avoid interacting with tzdata
RUN apt-get -qq -y update && \
    apt-get -qq -y upgrade && \
    DEBIAN_FRONTEND=noninteractive apt-get -qq -y install \
        gcc \
        g++ \
        zlibc \
        zlib1g-dev \
        libssl-dev \
        libbz2-dev \
        libsqlite3-dev \
        libncurses5-dev \
        libgdbm-dev \
        libgdbm-compat-dev \
        liblzma-dev \
        libreadline-dev \
        uuid-dev \
        libffi-dev \
        tk-dev \
        wget \
        curl \
        git \
	python3-pip \
	python3-tk \	
        make \
        sudo \
        bash-completion \
        tree \
        emacs \
        vim \
        graphviz \
        jq \
        hdf5-tools \
        software-properties-common && \
    mv /usr/bin/lsb_release /usr/bin/lsb_release.bak && \
    apt-get -y autoclean && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt-get/lists/*


COPY requirements.txt requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt && \
    rm requirements.txt

# Make images Grid / Singularity compatible
RUN mkdir -p /home/testarea

# Make images Grid / Singularity compatible
RUN mkdir -p /alrb /cvmfs /afs /eos

# Create user "atlas" with sudo powers
RUN useradd -m atlas && \
    usermod -aG sudo atlas && \
    echo '%sudo ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers && \
    mkdir /home/atlas/data && \
    chown -R --from=root atlas /home/atlas

# Start the image not in /root to protect config files
ENV HOME=/home/atlas
ENV USER=atlas

# Add h5ls tab completion
COPY _h5ls.sh /etc/
RUN chmod a+r /etc/_h5ls.sh

USER atlas
WORKDIR ${HOME}/data

# Avoid first use of sudo warning. c.f. https://askubuntu.com/a/22614/781671
RUN touch $HOME/.sudo_as_admin_successful

# Start the image with BASH by default
CMD /bin/bash --rcfile /home/atlas/.bashrc
